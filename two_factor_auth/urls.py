from django.urls import path, include
from two_factor.urls import urlpatterns as tf_urls

from . import views

app_name = 'two_factor_auth'
urlpatterns = [
    path('account/register/', views.register, name='register'),
    path('account/logout', views.log_out, name='logout'),
    path('', include(tf_urls)),
]
