from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout

from .forms import NewUserForm


def register(request):
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, "Registration successful.")
            return redirect("two_factor:profile")
    form = NewUserForm(request.POST)
    return render(request, 'two_factor_auth/register.html', context={"register_form": form})


def log_out(request):
    logout(request)
    return redirect("two_factor:login")
